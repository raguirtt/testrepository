﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var solutionobj = new Solution();
            int[] a = new int[] { 1, 2, 3 };

            var result = solutionobj.solution(a);
            Console.WriteLine("testing");
            Console.WriteLine(result);
            Console.Read();
        }

       
    }

    class Solution
    {
        public int solution(int[] A, string title, string content)
        {

            if (title == null || title.Length < 5 || title.Length > 20)
                throw new ArgumentException;

            var x = A.Where(i => i > 0).FirstOrDefault();
            if (x == 0)
                return 1;
            else
            {

                var maxVal = A.Max();
                int returnVal = 0;
                for(int j = 1; j <= maxVal; j++)
                {
                    var found = A.Where(k => k == j).FirstOrDefault();
                    if (found == 0)
                    {
                       
                        return j;
                    }
                    returnVal = j;
                }
                return ++returnVal;

            }

           

        }
    }
}
